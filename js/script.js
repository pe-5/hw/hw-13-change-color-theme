let link = document.querySelector('link[rel="stylesheet"]');
let localValue = localStorage.getItem('style');
if (localValue !== null) {
    link.setAttribute('href', localValue);
}

    let btn = document.getElementById('change-mode-btn');
    btn.onclick = function () {
        link.setAttribute('href', link.getAttribute('href') === "css/style.css" ? "css/dark-mode.css" : "css/style.css");
        let storageStyleValue = link.getAttribute('href');
        localStorage.setItem('style', storageStyleValue);
    };
